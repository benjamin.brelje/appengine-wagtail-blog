from django.core.exceptions import ValidationError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Count, Q
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailcore.models import Page
from wagtail.wagtailadmin.edit_handlers import (
    FieldPanel, InlinePanel, MultiFieldPanel, FieldRowPanel, StreamFieldPanel)
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsnippets.models import register_snippet
from wagtail.wagtailsearch import index
from taggit.models import TaggedItemBase, Tag
from modelcluster.tags import ClusterTaggableManager
from modelcluster.fields import ParentalKey
import datetime
from blog.blocks import CodeBlock


COMMENTS_APP = getattr(settings, 'COMMENTS_APP', None)
#these are from nesting-box for implementing the sreamblock
from django.forms import ChoiceField
#from django.utils.translation import ugettext as _

from wagtail.wagtailembeds.blocks import EmbedBlock
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailcore.blocks import (
    StructBlock, StreamBlock, CharBlock,
    RichTextBlock, TextBlock, FieldBlock, RawHTMLBlock
)


class ImageFormatBlock(FieldBlock):
    """
    The Image stream field accepts an ``alignment`` parameter that adds
    a class to the resulting image. This allows editors to place the image
    in different ways, according to the built-in CSS style.
    """
    FORMAT_CHOICES = (
        ('image-left', _('Wrap left')),
        ('image-right', _('Wrap right')),
        ('image-middle', _('Place in the center')),
        ('image-full', _('Full width')),
    )

    field = ChoiceField(choices=FORMAT_CHOICES)


class ImageBlock(StructBlock):
    """
    The ``aligned_image`` block, used to add an image into the ``BlogPage``
    """
    image = ImageChooserBlock()
    alignment = ImageFormatBlock()
    caption = RichTextBlock(required=False)


class PullQuoteBlock(StructBlock):
    """
    A block that adds a quote with the proper attribution
    """
    quote = TextBlock(label=_("Quote title"))
    attribution = CharBlock()


class VideoBlock(StructBlock):
    """
    A block that adds an embedded block for
    """
    video_link = EmbedBlock()


class BodyStreamBlock(StreamBlock):
    """
    Defines a ``StreamBlock`` with a dynamic behavior when writing the body of new pages.
    It includes:
        - h2 and h3 titles
        - a paragraph
        - an image that could be aligned on the left, on the right, in the center
          or with a full width
        - a quote with attributions
        - an embedded iframe for videos
    """
    h2 = CharBlock(icon="title")
    h3 = CharBlock(icon="title")
    paragraph = RichTextBlock(icon="pilcrow")
    video = VideoBlock(label=_("Embedded video"), icon="media")
    pullquote = PullQuoteBlock(label=_("Quote"), icon="openquote")
    rawhtml = RawHTMLBlock(label=_("Embed HTML"), icon="placeholder")
    code = CodeBlock(label=_("Embed code"), icon="code")

	
	#duplicate the staticpage class at home
class HomePage(Page):
    body = StreamField(BodyStreamBlock(), null=True)
    subtitle = models.TextField(max_length=600, null=True)
    header_image = models.TextField(max_length=600, null=True, blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]
    settings_panels = [
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('go_live_at'),
                FieldPanel('expire_at'),
            ], classname="label-above"),
        ], 'Scheduled publishing', classname="publishing"),
    ]

    def get_absolute_url(self):
        return self.url


    class Meta:
        verbose_name = _('Home page')
        verbose_name_plural = _('Home pages')


HomePage.content_panels = [
    FieldPanel('title', classname="full title"),
    FieldPanel('subtitle'),
    FieldPanel('header_image'),
    StreamFieldPanel('body'),
	
]

