# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-09 05:49
from __future__ import unicode_literals

import blog.models
from django.db import migrations
import wagtail.wagtailcore.blocks
import wagtail.wagtailcore.fields
import wagtail.wagtailembeds.blocks
import wagtail.wagtailimages.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20170608_2244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpage',
            name='intro',
            field=wagtail.wagtailcore.fields.StreamField([(b'h2', wagtail.wagtailcore.blocks.CharBlock(icon=b'title')), (b'h3', wagtail.wagtailcore.blocks.CharBlock(icon=b'title')), (b'paragraph', wagtail.wagtailcore.blocks.RichTextBlock(icon=b'pilcrow')), (b'image', wagtail.wagtailcore.blocks.StructBlock([(b'image', wagtail.wagtailimages.blocks.ImageChooserBlock()), (b'alignment', blog.models.ImageFormatBlock()), (b'caption', wagtail.wagtailcore.blocks.RichTextBlock(required=False))], icon=b'image', label='Aligned image')), (b'video', wagtail.wagtailcore.blocks.StructBlock([(b'video_link', wagtail.wagtailembeds.blocks.EmbedBlock())], icon=b'media', label='Embedded video')), (b'pullquote', wagtail.wagtailcore.blocks.StructBlock([(b'quote', wagtail.wagtailcore.blocks.TextBlock(label='Quote title')), (b'attribution', wagtail.wagtailcore.blocks.CharBlock())], icon=b'openquote', label='Quote'))], null=True),
        ),
    ]
