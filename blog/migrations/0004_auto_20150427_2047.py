# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields
import wagtail.wagtailcore.blocks
import django.db.models.deletion
import wagtail_box.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20150323_2116'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blogcategory',
            options={'ordering': ['name'], 'verbose_name_plural': 'Blog Categories', 'verbose_name': 'Blog Category'},
        ),
        migrations.AlterModelOptions(
            name='blogindexpage',
            options={'verbose_name': 'Blog index'},
        ),
        migrations.AlterModelOptions(
            name='blogpage',
            options={'verbose_name_plural': 'Blog pages', 'verbose_name': 'Blog page'},
        ),
        migrations.AddField(
            model_name='blogcategory',
            name='description',
            field=models.CharField(max_length=500, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogcategory',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, to='blog.BlogCategory', help_text='Categories, unlike tags, can have a hierarchy. You might have a Jazz category, and under that have children categories for Bebop and Big Band. Totally optional.'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogcategoryblogpage',
            name='category',
            field=models.ForeignKey(verbose_name='Category', related_name='+', to='blog.BlogCategory'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='body',
            field=wagtail.wagtailcore.fields.StreamField((('h2', wagtail.wagtailcore.blocks.CharBlock(icon='title')), ('h3', wagtail.wagtailcore.blocks.CharBlock(icon='title')), ('paragraph', wagtail.wagtailcore.blocks.RichTextBlock(icon='pilcrow')), ('image', wagtail.wagtailcore.blocks.StructBlock((('image', wagtail.wagtailimages.blocks.ImageChooserBlock()), ('alignment', wagtail_box.fields.ImageFormatBlock()), ('caption', wagtail.wagtailcore.blocks.RichTextBlock(required=False))), icon='image', label='Aligned image')), ('video', wagtail.wagtailcore.blocks.StructBlock((('video_link', wagtail.wagtailembeds.blocks.EmbedBlock()),), icon='media', label='Embedded video')), ('pullquote', wagtail.wagtailcore.blocks.StructBlock((('quote', wagtail.wagtailcore.blocks.TextBlock(label='Quote title')), ('attribution', wagtail.wagtailcore.blocks.CharBlock())), icon='openquote', label='Quote')))),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogpage',
            name='header_image',
            field=models.TextField(max_length=600, verbose_name='Header image', null=True, blank=True),
            preserve_default=True,
        ),
    ]
