# Welcome
This is the source for my website. It's built with the DJango framework, the Wagtail CMS, and Bootstrap themes.
It was a pain to customize for App Engine so I will eventually share my work.

I used the [wagtail-blog project](https://github.com/thelabnyc/wagtail_blog) but changed the RichTextField of the blog
post object with a StreamField and plan on adding some other customizations. I used the StreamField layout from [wagtail-nesting-box](https://github.com/palazzem/wagtail-nesting-box)

## Build details:

- Install the Google Cloud SDK
- Create a Cloud SQL instance
- Create a database in Cloud SQL (or two - one for prod, one for dev)
- Create an app engine project

- Create a virtualenv using requirements.txt
- In the project directory: pip install -t lib -r requirements-vendor.txt
- DELETE PIL and Pillow folders from the lib binaries folder (they conflict with the binaries provisioned by Google App Engine)

- Create an app.yaml file from the template in the repo. Fill in your secrets.
- Provide secrets. Add to environment variables in manage.py as follows (you need all the secrets in the .yaml):
os.environ['ENVIRONMENT VAR'] = 'value'

## Local Test
- python manage.py migrate
- python manage.py createsuperuser
- Start the dev server: python manage.py runserver (may want to use 'dev' settings rather than production)
- Visit your site at localhost:8000

## Deployment:
- Switch the database to production source if you haven't already
- python manage.py collectstatic
- python manage.py migrate
- python manage.py createsuperuser
- When ready, deploy using the GCloud SDK: "gcloud app deploy"

